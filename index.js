var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var players = [];
var bullets = [];

server.listen(8080, function () {
    console.log("Server is now running on port 8080...");
});

io.on('connection', function (socket) {
    console.log("Player Connected!");
    socket.emit('socketID', {id: socket.id});
    console.log("Number of players on server: " + players.length);
    socket.emit('getPlayers', players);
    socket.broadcast.emit('newPlayer', {id: socket.id});
    socket.on('playerMoved', function (data) {
        try {
			data.id = socket.id;
			socket.broadcast.emit('playerMoved', data);
			for (var i = 0; i < players.length; i++) {
				if (players[i].id === data.id) {
					players[i].x = data.x;
					players[i].y = data.y;
				}
			}
        } catch (e) {
            console.error("Error moving Player on server", e);
        }
    });
	socket.on('playerShot', function (data) {
		try {
			data.id = socket.id;
			bullets.push(new Bullet(data.id, data.x, data.y, data.angle, data.side));
			console.log("Bullet Added");
			socket.broadcast.emit('playerShot', data);
        } catch (e) {
            console.error("Error shooting on server", e);
        }
	});
    socket.on('playerSwitchSide', function (data) {
        try {
            console.log('Player ' + data.id + ' switch side to: ' + data.side);
            for (var i = 0; i < players.length; i++) {
				if (players[i].id === data.id) {
                    players[i].side = data.side;
				}
			}
            socket.broadcast.emit('playerSwitchSide', data);
        } catch(e) {
            console.error('Erro getting player to switch sides', e);
        }
    });
	socket.on('bulletRemoved', function () {
		try {
			console.log("Bullet Removed");
			for (var i = 0; i < bullets.length; i++) {
				if (bullets[i].id === socket.id) {
					bullets.splice(i, 1);
				}
			}
		} catch (e) {
			console.error("Error removing Bullet from server", e);
		}
	});
	socket.on('playerHit', function (data) {
		try {
			console.log("Player got hit");
			socket.broadcast.emit('playerHit', data);
		} catch (e) {
			console.error("Error getting Player hit on server", e);
		}
	});
    socket.on('disconnect', function () {
        console.log("Player Disconnected");
        socket.broadcast.emit('playerDisconnected', {id: socket.id});
        for (var i = 0; i < players.length; i++) {
            if (players[i].id === socket.id) {
                console.log("Player removed: " + players[i].id);
                players.splice(i, 1);
            }
        }
    });
    players.push(new Player(socket.id, 0, 0));
});

function Player(id, x, y) {
    this.id = id;
    this.x = x;
    this.y = y;

    /**
     * The side:
     *
     * 0 - right
     * 1- left
     */
    this.side = 0;
}

function Bullet(id, x, y, angle, side) {
	this.id = id;
	this.x = x;
	this.y = y;
	this.angle = angle;

    /**
     * The side:
     *
     * 0 - right
     * 1- left
     */
    this.side = side;
}
